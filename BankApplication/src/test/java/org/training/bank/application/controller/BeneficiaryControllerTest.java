package org.training.bank.application.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.service.BeneficiaryService;

@ExtendWith(SpringExtension.class)
class BeneficiaryControllerTest {

	@InjectMocks
	private BeneficiaryController controller;
	
	@Mock
	private BeneficiaryService service;
	
	@Test
	void fundTransfer() {
		
		ResponseDto response = ResponseDto.builder()
				.responseCode("200")
				.responseMessage("Fund transfer made successfully").build();
		
		Mockito.when(service.beneficiary(Mockito.any())).thenReturn(response);
		
		ResponseEntity<ResponseDto> result = controller.beneficiary(Mockito.any(BeneficiaryDto.class));
		assertNotNull(result);
	}
}
