package org.training.bank.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.RegisterDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.AccountType;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.IfscCode;
import org.training.bank.application.exception.PasswordMatchException;
import org.training.bank.application.exception.ResourceConflictExists;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class CustomerServiceImplTest {

	@InjectMocks
	private CustomerServiceImpl customerService;
	
	@Mock
	private CustomerRepository customerRepository;
	
	@Mock
	private BeneficiaryAccountRepository beneficiaryAccountRepository;
	
	@Mock
	private AccountRepository accountRepository;
	
	@Test
	void login_UnAuthorizedUser() {
		
		LoginDto loginDto = new LoginDto();
		Mockito.when(customerRepository.findByCustomerIdAndPassword(Mockito.anyString(), Mockito.anyString()))
			.thenReturn(Optional.empty());
		
		UnAuthorizedAccess exception = assertThrows(UnAuthorizedAccess.class, 
				() -> customerService.login(loginDto));
	}
	
	@Test 
	void login_Success() {
		
		LoginDto loginDto = LoginDto.builder()
				.customerId("465645")
				.password("Vikram@145636").build();
	
		Customer customer = Customer.builder().customerId("7687684l")
				.password("Vikram@145636").account(Account.builder()
						.accountNumber("625375798")
						.accountType(AccountType.SAVINGS).ifscCode(IfscCode.UTIB098).build()).build();
		
		Mockito.when(customerRepository.findByCustomerIdAndPassword(Mockito.anyString(), Mockito.anyString()))
			.thenReturn(Optional.of(customer));
		
		AccountDto accountDto = customerService.login(loginDto);
		assertNotNull(accountDto);
	}
	
	@Test
	void customerRegisteredAlreadyExist() {
		Customer customer = Customer.builder().emailId("megha@gmail.com").contactNo("7760384928").build();
		
		Mockito.when(customerRepository.findByEmailIdOrContactNo("megha@gmail.com","7760384928")).thenReturn(Optional.of(customer));
		RegisterDto registerDto = RegisterDto.builder().emailId("megha@gmail.com").contactNo("7760384928").password("megha@1234").confirmPassword("megha@1234").build();

		assertThrows(ResourceConflictExists.class, () -> customerService.register(registerDto));

	}
	
	@Test
	void customerPasswordNotMatch() {
		Customer customer = Customer.builder().emailId("megha@gmail.com").contactNo("7760384928").build();
		
		Mockito.when(customerRepository.findByEmailIdOrContactNo("megha@gmail.com","7760384928")).thenReturn(Optional.of(customer));
		RegisterDto registerDto = RegisterDto.builder().emailId("megha@gmail.com").contactNo("7760384928").password("megha@1234").confirmPassword("meha@1234").build();

		assertThrows(PasswordMatchException.class, () -> customerService.register(registerDto));

	}
	
	@Test
	void customerRegisteredSuccess() {
		Customer customer = Customer.builder().emailId("megha@gmail.com").contactNo("7760384928").build();
		Mockito.when(customerRepository.findByEmailIdOrContactNo("megha@gmail.com","7760384928")).thenReturn(Optional.empty());

		Mockito.when(accountRepository.count()).thenReturn(1l);
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		RegisterDto registerDto = RegisterDto.builder().emailId("megha@gmail.com").contactNo("7760384928").password("megha@134").confirmPassword("megha@134").build();
		ResponseDto apiResponse = customerService.register(registerDto);

		assertEquals("Customer Registered sucessfully", apiResponse.getResponseMessage());

	}
	
}
