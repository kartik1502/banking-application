package org.training.bank.application.controller;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.RegisterDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.service.CustomerService;

@ExtendWith(SpringExtension.class)
class RegisterControllerTest {

	@InjectMocks
	private RegisterController controller;
	
	@Mock
	private CustomerService service;
	
	@Test
	void fundTransfer() {
		
		ResponseDto response = ResponseDto.builder()
				.responseCode("200")
				.responseMessage("Fund transfer made successfully").build();
		
		Mockito.when(service.register(Mockito.any())).thenReturn(response);
		
		ResponseEntity<ResponseDto> result = controller.register(Mockito.any(RegisterDto.class));
		assertNotNull(result);
	}
}
