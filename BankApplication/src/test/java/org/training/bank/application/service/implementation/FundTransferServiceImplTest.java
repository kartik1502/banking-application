package org.training.bank.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.FundTransferDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.InSufficientFunds;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.repository.FundTransferRepository;

@ExtendWith(SpringExtension.class)
class FundTransferServiceImplTest {
	
	@InjectMocks
	private FundTransferServiceImpl fundTransferService;
	
	@Mock
	private CustomerRepository customerRepository;
	
	@Mock
	private AccountRepository accountRepository;
	
	@Mock
	private BeneficiaryAccountRepository beneficiaryAccountRepository;
	
	@Mock
	private FundTransferRepository fundTransferRepository;
	
	@Test
	void testFundTransfer_FromAccoutNotFound() {
		
		Mockito.when(accountRepository.findAccountByAccountNumber(Mockito.anyString()))
		.thenReturn(Optional.empty());
		
		FundTransferDto fundTransferDto = new FundTransferDto();
		ResourceNotFound exception =  assertThrows(ResourceNotFound.class,
				() -> fundTransferService.fundTransfer(fundTransferDto));
	}
	
	@Test
	void testFundTransfer_CustomerNotFound() {
		
		Account account = Account.builder()
				.accountNumber("428765hkjh")
				.balance(BigDecimal.valueOf(65)).build();
		
		Mockito.when(accountRepository.findAccountByAccountNumber(Mockito.anyString()))
		.thenReturn(Optional.of(account));
		
		
		Mockito.when(customerRepository.findByAccount(account))
		.thenReturn(Optional.empty());
		
		FundTransferDto fundTransferDto = FundTransferDto.builder()
				.fromAccountNumber("428765hkjh").build();
		
		ResourceNotFound exception =  assertThrows(ResourceNotFound.class,
				() -> fundTransferService.fundTransfer(fundTransferDto));
	}
	
	@Test
	void testFundTransfer_CustomerNotLoggedIn() {
		
		Account account = Account.builder()
				.accountNumber("428765hkjh")
				.balance(BigDecimal.valueOf(65)).build();
		
		Mockito.when(accountRepository.findAccountByAccountNumber(Mockito.anyString()))
		.thenReturn(Optional.of(account));
		
		Customer customer = Customer.builder()
				.name("kartik").loginStatus(LoginStatus.LOGGEDOUT).account(account).build();
		
		Mockito.when(customerRepository.findByAccount(account))
		.thenReturn(Optional.of(customer));
		
		FundTransferDto fundTransferDto = FundTransferDto.builder()
				.fromAccountNumber("428765hkjh").build();
		
		UnAuthorizedAccess exception =  assertThrows(UnAuthorizedAccess.class,
				() -> fundTransferService.fundTransfer(fundTransferDto));
	}
	
	@Test
	void testFundTransfer_InSufficientFunds() {
		
		Account account = Account.builder()
				.accountNumber("428765hkjh").balance(BigDecimal.valueOf(100))
				.balance(BigDecimal.valueOf(65)).build();
		
		Mockito.when(accountRepository.findAccountByAccountNumber(Mockito.anyString()))
		.thenReturn(Optional.of(account));
		
		Customer customer = Customer.builder()
				.name("kartik").loginStatus(LoginStatus.LOGGEDIN).account(account).build();
		
		Mockito.when(customerRepository.findByAccount(account))
		.thenReturn(Optional.of(customer));
		
		FundTransferDto fundTransferDto = FundTransferDto.builder()
				.fromAccountNumber("428765hkjh").amount(BigDecimal.valueOf(500)).build();
		
		InSufficientFunds exception =  assertThrows(InSufficientFunds.class,
				() -> fundTransferService.fundTransfer(fundTransferDto));
	}
	
	@Test
	void testFundTransfer_BeneficiaryNotFound() {
		
		Account account = Account.builder()
				.accountNumber("428765hkjh").balance(BigDecimal.valueOf(10000))
				.balance(BigDecimal.valueOf(65)).build();
		
		Mockito.when(accountRepository.findAccountByAccountNumber(Mockito.anyString()))
		.thenReturn(Optional.of(account));
		
		Customer customer = Customer.builder()
				.name("kartik").loginStatus(LoginStatus.LOGGEDIN).account(account).build();
		
		Mockito.when(customerRepository.findByAccount(account))
		.thenReturn(Optional.of(customer));
		
		FundTransferDto fundTransferDto = FundTransferDto.builder()
				.fromAccountNumber("428765hkjh").toAccountNumber("5678gh").amount(BigDecimal.valueOf(5)).build();
		
		Mockito.when(beneficiaryAccountRepository.findByAccountNumber(fundTransferDto.getToAccountNumber()))
		.thenReturn(Optional.empty());
		
		ResourceNotFound exception =  assertThrows(ResourceNotFound.class,
				() -> fundTransferService.fundTransfer(fundTransferDto));
	}
	
	@Test
	void fundTransfer_Success() {
		
		Account account = Account.builder()
				.accountNumber("428765hkjh").balance(BigDecimal.valueOf(10000))
				.balance(BigDecimal.valueOf(65)).build();
		
		Mockito.when(accountRepository.findAccountByAccountNumber(Mockito.anyString()))
		.thenReturn(Optional.of(account));
		
		Customer customer = Customer.builder()
				.name("kartik").loginStatus(LoginStatus.LOGGEDIN).account(account).build();
		
		Mockito.when(customerRepository.findByAccount(account))
		.thenReturn(Optional.of(customer));
		
		FundTransferDto fundTransferDto = FundTransferDto.builder().accountType("DEBIT")
				.fromAccountNumber("428765hkjh").toAccountNumber("5678gh").amount(BigDecimal.valueOf(5)).build();
		
		BeneficiaryAccount beneficiaryAccount = BeneficiaryAccount.builder()
				.accountNumber("5678gh")
				.balance(BigDecimal.valueOf(67)).build();
		
		Mockito.when(beneficiaryAccountRepository.findByAccountNumber(fundTransferDto.getToAccountNumber()))
		.thenReturn(Optional.of(beneficiaryAccount));
		
		ResponseDto responseDto = fundTransferService.fundTransfer(fundTransferDto);
		assertNotNull(responseDto);
		assertEquals("Fund transfer made successfully", responseDto.getResponseMessage());
	}

	
}
