package org.training.bank.application.service.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;

@ExtendWith(SpringExtension.class)
class BeneficiaryServiceImplTest {
	
	@InjectMocks
	private BeneficiaryServiceImpl beneficiaryService;
	
	@Mock
	private CustomerRepository customerRepository;
	
	@Mock
	private BeneficiaryAccountRepository beneficiaryAccountRepository;
	
	@Test
	void customerNotFound()
	{
		Mockito.when(customerRepository.findByCustomerId("2")).thenReturn(Optional.empty());
		BeneficiaryDto beneficiaryDto = BeneficiaryDto.builder().customerId("2").accountHolderName("megha").accountNumber("sbi12345").balance(BigDecimal.valueOf(2000)).branchName("abc").build();

		assertThrows(ResourceNotFound.class, () -> beneficiaryService.beneficiary(beneficiaryDto));
	}
	
	@Test
	void beneficiaryAccount_LoggedOut() {
		
		Customer customer = Customer.builder().emailId("megha@gmail.com").loginStatus(LoginStatus.LOGGEDOUT).contactNo("7760384928").build();
		
		Mockito.when(customerRepository.findByCustomerId("2")).thenReturn(Optional.of(customer));
		BeneficiaryAccount beneficiaryAccount = BeneficiaryAccount.builder().branchName("abc").beneficiaryId(2l).balance(BigDecimal.valueOf(2000)).accountNumber("sbi12345").accountHolderName("megha").build();
		Mockito.when(beneficiaryAccountRepository.findByCustomerId(2l)).thenReturn(Optional.of(beneficiaryAccount));

		Mockito.when(beneficiaryAccountRepository.save(beneficiaryAccount)).thenReturn(beneficiaryAccount);
		BeneficiaryDto beneficiaryDto =BeneficiaryDto.builder().customerId("2").branchName("abc").balance(BigDecimal.valueOf(2000)).accountNumber("sbi12345").accountHolderName("megha").build();
		assertThrows(UnAuthorizedAccess.class, () -> beneficiaryService.beneficiary(beneficiaryDto));

	}
	
	@Test
	void customerBeneficiaryAccountSuccess() {
		
		Customer customer = Customer.builder().emailId("megha@gmail.com").loginStatus(LoginStatus.LOGGEDIN).contactNo("7760384928").build();
		
		Mockito.when(customerRepository.findByCustomerId("2")).thenReturn(Optional.of(customer));
		BeneficiaryAccount beneficiaryAccount = BeneficiaryAccount.builder().branchName("abc").beneficiaryId(2l).balance(BigDecimal.valueOf(2000)).accountNumber("sbi12345").accountHolderName("megha").build();
		Mockito.when(beneficiaryAccountRepository.findByCustomerId(2l)).thenReturn(Optional.of(beneficiaryAccount));

		Mockito.when(beneficiaryAccountRepository.save(beneficiaryAccount)).thenReturn(beneficiaryAccount);
		BeneficiaryDto beneficiaryDto =BeneficiaryDto.builder().customerId("2").branchName("abc").balance(BigDecimal.valueOf(2000)).accountNumber("sbi12345").accountHolderName("megha").build();
		ResponseDto apiResponse = beneficiaryService.beneficiary(beneficiaryDto);

		assertEquals("Beneficiary Account created", apiResponse.getResponseMessage());

	}

}
