package org.training.bank.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.FundTransferDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.service.FundTransferService;


import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/fund-transfers")
@RequiredArgsConstructor
public class FundTransferController {

	private final FundTransferService fundTransferService;
	
	@PostMapping
	public ResponseEntity<ResponseDto> fundTransfer(@RequestBody @Valid FundTransferDto fundTransferDto) {
		log.info("Intiating the fund transfer....");
		return new ResponseEntity<>(fundTransferService.fundTransfer(fundTransferDto), HttpStatus.CREATED);
	}
}
