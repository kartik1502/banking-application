package org.training.bank.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.service.CustomerService;


import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class LoginController {

	private final CustomerService customerService;
	
	@PutMapping
	public ResponseEntity<AccountDto> login(@RequestBody @Valid LoginDto loginDto) {
		log.info("Logging in the customer......");
		return new ResponseEntity<>(customerService.login(loginDto), HttpStatus.OK);
	}
}
