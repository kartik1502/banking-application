package org.training.bank.application.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {

	private String accountNumber;
	
	private String ifscCode;
	
	private BigDecimal balance;
	
	private String accountType;
}
