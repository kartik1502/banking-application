package org.training.bank.application.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.training.bank.application.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long> {
	
	Optional<Account> findAccountByAccountNumber(String accountNumber);
	
	@Query("select count(*) from Account")
	long count();

}
