package org.training.bank.application.exception;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Value("${spring.application.bad.request}")
	private String badRequest;
	
	@Value("${spring.application.conflict}")
	private String conflict;
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {

		String message = ex.getLocalizedMessage();
		ErrorResponse response = new ErrorResponse("400", message);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UnAuthorizedAccess.class)
	public ResponseEntity<ErrorResponse> handleUnAuthorizedAccess(UnAuthorizedAccess ex) {
		return new ResponseEntity<>(new ErrorResponse(badRequest, ex.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(InSufficientFunds.class)
	public ResponseEntity<ErrorResponse> handleInSufficientFunds(InSufficientFunds ex) {
		return new ResponseEntity<>(new ErrorResponse(badRequest, ex.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(PasswordMatchException.class)
	public ResponseEntity<ErrorResponse> handlePasswordMatchException(PasswordMatchException ex) {
		return new ResponseEntity<>(new ErrorResponse(badRequest, ex.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(ResourceNotFound.class)
	public ResponseEntity<ErrorResponse> handleResourceNotFound(ResourceNotFound ex) {
		return new ResponseEntity<>(new ErrorResponse(conflict, ex.getLocalizedMessage()), HttpStatus.CONFLICT);
	}
	
	


}
