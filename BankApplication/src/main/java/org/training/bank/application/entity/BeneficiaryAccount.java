package org.training.bank.application.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class BeneficiaryAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long beneficiaryId;
	
	private String accountNumber;
	
	private String ifscCode;
	
	private BigDecimal balance;
	
	private String branchName;
	
	private String accountHolderName;
	
	@ManyToOne
	@JoinColumn(name = "customerId")
	private Customer customer;
}