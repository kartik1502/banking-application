package org.training.bank.application.service;

import org.training.bank.application.dto.AccountDto;
import org.training.bank.application.dto.LoginDto;
import org.training.bank.application.dto.RegisterDto;
import org.training.bank.application.dto.ResponseDto;

import jakarta.validation.Valid;

public interface CustomerService {

	AccountDto login(LoginDto loginDto);
	
	ResponseDto register(@Valid RegisterDto registerDto);
}