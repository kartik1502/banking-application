package org.training.bank.application.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Optional<Customer> findByCustomerIdAndPassword(String emailId, String password);
	
	Optional<Customer> findByAccount(Account account);
	
	Optional<Customer> findByEmailIdOrContactNo(String emailId,String contactNo);

	Optional<Customer> findByCustomerId(String customerId);
	
	@Query("select count(*) from Customer")
	long count();
}