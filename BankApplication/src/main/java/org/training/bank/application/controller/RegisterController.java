package org.training.bank.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.RegisterDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.service.CustomerService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequiredArgsConstructor
public class RegisterController {
	
	private final CustomerService customerService;
	
	@PostMapping("/customers")
	public ResponseEntity<ResponseDto> register(@RequestBody @Valid RegisterDto registerDto){
		log.info("registering the customer.....");
		return ResponseEntity.status(HttpStatus.CREATED).body(customerService.register(registerDto));
	}

}
