package org.training.bank.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.training.bank.application.entity.FundTransfer;

public interface FundTransferRepository extends JpaRepository<FundTransfer, Long> {

}
