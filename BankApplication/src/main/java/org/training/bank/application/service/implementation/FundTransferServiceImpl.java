package org.training.bank.application.service.implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.training.bank.application.dto.FundTransferDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.entity.Account;
import org.training.bank.application.entity.BeneficiaryAccount;
import org.training.bank.application.entity.Customer;
import org.training.bank.application.entity.FundTransfer;
import org.training.bank.application.entity.LoginStatus;
import org.training.bank.application.entity.TransferType;
import org.training.bank.application.exception.InSufficientFunds;
import org.training.bank.application.exception.ResourceNotFound;
import org.training.bank.application.exception.UnAuthorizedAccess;
import org.training.bank.application.repository.AccountRepository;
import org.training.bank.application.repository.BeneficiaryAccountRepository;
import org.training.bank.application.repository.CustomerRepository;
import org.training.bank.application.repository.FundTransferRepository;
import org.training.bank.application.service.FundTransferService;

import jakarta.transaction.Transaction;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class FundTransferServiceImpl implements FundTransferService {

	private final CustomerRepository customerRepository;

	private final AccountRepository accountRepository;
	
	private final BeneficiaryAccountRepository beneficiaryAccountRepository;
	
	private final FundTransferRepository fundTransferRepository;
	
	@Value("${spring.application.ok}")
	private String responseOk;

	@Override
	public ResponseDto fundTransfer(@Valid FundTransferDto fundTransferDto) {

		Account fromAccount = accountRepository.findAccountByAccountNumber(fundTransferDto.getFromAccountNumber())

				.orElseThrow(() -> {
					log.warn("Requested Account not found");
					return new ResourceNotFound("Requested from Account not found");
				});
		
		Customer customer = customerRepository.findByAccount(fromAccount)
				.orElseThrow(() -> {
					log.warn("Customer not found");
					return new ResourceNotFound("Requested Customer not found");
				});
		
		if(customer.getLoginStatus().equals(LoginStatus.LOGGEDOUT)) {
			log.warn("Unauthorized access, login to make fund transfers");
			throw new UnAuthorizedAccess("Unauthorized: Login to make fund transfers");
		}

		if (fromAccount.getBalance().compareTo(fundTransferDto.getAmount()) < 0) {
			log.warn("InSufficent funds in the account");
			throw new InSufficientFunds("InSufficent funds in the account");
		}

		BeneficiaryAccount toAccount = beneficiaryAccountRepository.findByAccountNumber(fundTransferDto.getToAccountNumber())

				.orElseThrow(() -> {
					log.warn("Requested Account not found");
					return new ResourceNotFound("Requested to Account not found");
				});
		
		log.info("Account checks done, waiting for the transfer");
		fromAccount.setBalance(fromAccount.getBalance().subtract(fundTransferDto.getAmount()));
		toAccount.setBalance(toAccount.getBalance().add(fundTransferDto.getAmount()));
		
		accountRepository.save(fromAccount);
		beneficiaryAccountRepository.save(toAccount);
		FundTransfer fundTransfer = FundTransfer.builder()
				.fromAccount(fromAccount.getAccountNumber())
				.toAccount(toAccount.getAccountNumber())
				.amount(fundTransferDto.getAmount())
				.transferType(TransferType.valueOf(fundTransferDto.getAccountType()))
				.build();
		
		fundTransferRepository.save(fundTransfer);
		log.info("Fund transfer done successfully");
		return ResponseDto.builder().responseCode(responseOk)
				.responseMessage("Fund transfer made successfully").build();
	}

}
