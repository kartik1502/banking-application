package org.training.bank.application.dto;

import java.math.BigDecimal;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BeneficiaryDto {
	
	@NotEmpty
	private String customerId;
	
	@NotEmpty
	private String accountNumber;

	@NotEmpty
	private String ifscCode;

	private BigDecimal balance;

	private String branchName;

	private String accountHolderName;

	private String type;

}
