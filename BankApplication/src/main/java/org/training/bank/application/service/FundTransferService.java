package org.training.bank.application.service;

import org.training.bank.application.dto.FundTransferDto;
import org.training.bank.application.dto.ResponseDto;

import jakarta.validation.Valid;

public interface FundTransferService {

	ResponseDto fundTransfer(@Valid FundTransferDto fundTransferDto);

}
