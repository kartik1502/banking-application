package org.training.bank.application.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
	
	@Pattern(regexp = "[a-zA-Z ]+", message = "name should have only alphabets")
	@NotBlank(message = "Name is requiredField")
	private String name;
   
	@Pattern(regexp = "[0-9]{12}")
	private String aadhar;
	
	@Email(message = "invalid email")
	@NotBlank
	private String emailId;
	
	@NotBlank(message = "mobileNumber is required")
	@Pattern(regexp = "[6-9][0-9]{9}", message = "Invalid contact number")
	private String contactNo;
	
	@NotBlank(message = "specify password")
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$@!%&*?])[A-Za-z\\d#$@!%&*?]{8,}$", message = "Password must contain atleast 1 uppercase letter, 1 lowercase letter,  1 special character, 1 number, Min 8 characters.")
	private String password;
	
	@NotBlank(message = "specify password")
	@Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[#$@!%&*?])[A-Za-z\\d#$@!%&*?]{8,}$", message = "Password must contain atleast 1 uppercase letter, 1 lowercase letter,  1 special character, 1 number, Min 8 characters.")
	private String confirmPassword;

}
