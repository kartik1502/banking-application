package org.training.bank.application.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.training.bank.application.dto.BeneficiaryDto;
import org.training.bank.application.dto.ResponseDto;
import org.training.bank.application.service.BeneficiaryService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/beneficiary-accounts")
@RequiredArgsConstructor
public class BeneficiaryController {

	private final BeneficiaryService beneficiaryService;
	
	@PostMapping
	public ResponseEntity<ResponseDto> beneficiary(@Valid @RequestBody BeneficiaryDto beneficiaryDto){
		log.info("Adding the beneficiary account.......");
		return ResponseEntity.status(HttpStatus.CREATED).body(beneficiaryService.beneficiary(beneficiaryDto));
	}
}